#!/bin/ksh
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

# Basis set
CRIT1=`$GREP "O * 2 * 8\.0000 * 65 * 15 * \[14s9p4d\|3s2p1d\]" $log | wc -l`
CRIT2=`$GREP "total\: * 2 * 16\.0000 * 130 * 30" $log | wc -l`
CRIT3=`$GREP "Cartesian basis used" $log | wc -l`
TEST[1]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=3
ERROR[1]="BASIS SET NOT READ CORRECTLY"

# Geometry
CRIT1=`$GREP "Total number of coordinates\: * 6" $log | wc -l`
CRIT2=`$GREP "1 * x * (0| )\.0000000000" $log | wc -l`
CRIT3=`$GREP "2 * y * (0| )\.0000000000" $log | wc -l`
CRIT4=`$GREP "3 * z * 1\.1405000000" $log | wc -l`
CRIT5=`$GREP "4 * x * (0| )\.0000000000" $log | wc -l`
CRIT6=`$GREP "5 * y * (0| )\.0000000000" $log | wc -l`
CRIT7=`$GREP "6 * z * \-1\.1405000000" $log | wc -l`
TEST[2]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7`
CTRL[2]=7
ERROR[2]="GEOMETRY NOT READ CORRECTLY"

# Symmetry
CRIT1=`$GREP "Number of coordinates in each symmetry\: * 1 * 1 * 1 * 0 * 1 * 1 * 1 * 0" $log | wc -l`
CRIT2=`$GREP "Number of orbitals in each symmetry\: * 8 * 3 * 3 * 1 * 8 * 3 * 3 * 1" $log | wc -l`
TEST[3]=`expr	$CRIT1 \+ $CRIT2`
CTRL[3]=2
ERROR[3]="SYMMETRY NOT CORRECT"

# Spin-orbit integrals
CRIT1=`$GREP "Two\-electron spin\-orbit integrals" $log | wc -l`
TEST[4]=`expr	$CRIT1`
CTRL[4]=1
ERROR[4]="SPIN-ORBIT INTEGRALS NOT CALCULATED"

# Energy
CRIT1=`$GREP "Final MCSCF energy\: * \-149\.556075023[4-5][0-9][0-9]" $log | wc -l`
TEST[5]=`expr	$CRIT1`
CTRL[5]=1
ERROR[5]="ENERGY NOT CORRECT"

# Response calculation
CRIT1=`$GREP "1 Excitation energies are calculated for symmetry no\. * 4" $log | wc -l`
CRIT2=`$GREP "2 property residues are calculated with labels\:" $log | wc -l`
TEST[6]=`expr	$CRIT1 \+ $CRIT2`
CTRL[6]=2
ERROR[6]="RESPONSE CALCULATION NOT SET UP CORRECTLY"

# Transition moments
CRIT1=`$GREP "\@ *STATE NO\: * 1 \*TRANSITION MOMENT\: *\-*1\.30258[0-9]*(D|E)\-03 \*ENERGY\(eV\)\: * \-2\.913258" $log | wc -l`
CRIT2=`$GREP "\@ *STATE NO\: * 1 \*TRANSITION MOMENT\: *\-*4\.54882[0-9]*(D|E)\-04 \*ENERGY\(eV\)\: * \-2\.913258" $log | wc -l`
TEST[7]=`expr	$CRIT1 \+ $CRIT2`
CTRL[7]=2
ERROR[7]="TRANSITION MOMENTS NOT CORRECT"

# Excitation energy
CRIT1=`$GREP "\@ *Excitation energy \: (\-0| \-)\.1070602[0-9] * au" $log | wc -l`
TEST[8]=`expr	$CRIT1`
CTRL[8]=1
ERROR[8]="EXCITATION ENERGY NOT CORRECT"

# Spin-orbit coupling
CRIT1=`$GREP "\@ *Spin\-orbit coupling constant \(Z1SPNORB\) \: * \-*285\.8836[0-9]* * cm\-1" $log | wc -l`
CRIT2=`$GREP "\@ *Spin\-orbit coupling constant \(Z2SPNORB\) \: * \-*99\.8350[0-9]* * cm\-1" $log | wc -l`
TEST[9]=`expr	$CRIT1 \+ $CRIT2`
CTRL[9]=2
ERROR[9]="SPIN-ORBIT COUPLING NOT CORRECT"


PASSED=1
for i in 1 2 3 4 5 6 7 8 9
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

